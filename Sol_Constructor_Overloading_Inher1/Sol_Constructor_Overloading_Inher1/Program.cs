﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Constructor_Overloading_Inher1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Sahitya sahityaObj = new Sahitya("diwik");

            //Presty prestyObj1 = new Presty();
            Presty prestyObj = new Presty("diwik", "suvarna");
        }
    }

    public class Sahitya
    {
        public Sahitya()
        {
            Console.WriteLine("sahitya-constuctor");
        }

        public Sahitya(string firstName) : this()
        {
            Console.WriteLine(firstName);
        }
    }

    public class Presty : Sahitya
    {
        public Presty() : base()
        {
            Console.WriteLine("presty-constuctor");
        }

        public Presty(string firstName, string lastName) : base(firstName)
        {
            Console.WriteLine(lastName);
        }
    }
}
